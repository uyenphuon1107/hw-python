
def Uppercase(s):
    alpha = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    result = ''
    for x in s:
        for position in range(52):
            if alpha[position] == x:
                i = position
        if x not in alpha or i>=26:
            result += x
        else:
            result += alpha[i+26]
    return result
print("Question 1")
print(Uppercase('abcd'))

def Lowercase(s):
    alpha = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    result = ''
    for x in s:
        for position in range(52):
            if alpha[position] == x:
                i = position
        if x not in alpha or i<=26:
            result += x
        else:
            result += alpha[i-26]
    return result
print("\nQuestion 2")
print(Lowercase('eksu'))

def Check_alpha(s):
    alpha = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    for x in s:
        if x in alpha:          
            return True
        return False
print("\nQuestion 3")
print("Test for bealHSY: ",(Check_alpha("bealHSY")))
print("Test for 42: ",(Check_alpha("42")))

def Check_digit(s):
    nums = '0123456789'
    for x in s:
        if x in nums:          
            return True
        return False
print("\nQuestion 4")
print("Test for bsad: ", Check_digit("bsad"))
print("Test for 34: ", Check_digit("34"))


def Check_specialchar(s):
    Special_Char = '!@#$%^&*()_+-={[}]:,.<>/?'
    for x in s:
        if x in Special_Char:          
            return True
        return False
print("\nQuestion 5")
print("Test for =: ",(Check_specialchar("=")))
print("Test for ab: ",(Check_specialchar("ab")))